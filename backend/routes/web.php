<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('vendas', 'Vendas');

/*
Route::prefix('/vendedores')->group(function() {
    Route::get('', 'Vendedores@index');
    Route::get('{$id}', 'Vendedores@getVendedor');
    Route::post('{$id}', 'Vendedores@setVendedor');
});*/

Route::resource('vendedores', 'Vendedores');

Route::get('bootstrap', function () {
    return view('inicial');
});
