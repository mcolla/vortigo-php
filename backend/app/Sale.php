<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['seller_id', 'price'];

    public function seller()
    {
        return $this->belongsTo('App\Seller');
    }
}
