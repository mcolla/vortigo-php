<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sale;
use App\Seller;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::all();
        return json_encode($sales);
    }

    /**
     * Store a newly seller.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sale = new Sale();
        $sale->seller_id = $request->seller_id;
        $sale->price = $request->price;
        $sale->save();

        $res = ["id"=>$sale->id,
                "seller_id"=>$sale->seller_id,
                "price"=>$request->price,
                "comissao"=>$this->comissao($request->price),
                "data_venda"=>$sale->created_at];
        return json_encode($res);
    }

    /**
     * Display the specified sale.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::find($id);
        return json_encode($sale);
    }

    /**
     * Update the sale.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sale = Sale::find($id);
        if (isset($sale)) {
            $sale->name = $request->name;
            $sale->email = $request->email;
            $sale->save();
            return json_encode($sale);
            return response('OK', 200);
        }
    }

    /**
     * Display sales of seller
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAllSales($seller_id)
    {
        $sales = Sale::where('seller_id',$seller_id)->get();

        $vlrTotal = 0;
        foreach ($sales as $s){
            $vlrTotal+= $s['price'];
        }

        $comissao = $this->comissao($vlrTotal);
        return json_encode(["vendas"=>$sales, "valor_total"=>$vlrTotal, "comissao"=> $comissao]);
    }


    public function comissao($price){
        $porcentagem = 6.5;
        $res = $price * ($porcentagem / 100);
        return $res;
    }
}
