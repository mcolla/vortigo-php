<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Seller;
use App\Sale;

class SellerController extends Controller
{
    /**
     * Display a listing of the sellers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::all();

        foreach ($sellers as $seller) {
            $sales = Sale::where('seller_id',$seller->id)->get();

            $comissao = 0;
            $vlrTotal = 0;
            foreach ($sales as $s){
                $vlrTotal+= $s['price'];
            }
            $comissao = $this->comissao($vlrTotal);
            $res[] = [
                "nome"=>$seller->name,
                "email"=>$seller->email,
                "comissao"=> $comissao];
        }
        return json_encode($res);
    }

    /**
     * Store a newly seller.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seller = new Seller();
        $seller->name = $request->name;
        $seller->email = $request->email;
        $seller->save();

        $res = ["id"=>$seller->id,
                "nome"=>$request->name,
                "email"=>$request->email];
        return json_encode($res);
    }

    /**
     * Display the specified seller.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seller = Seller::find($id);
        return json_encode($seller);
    }

    /**
     * Update the seller.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller = Seller::find($id);
        if (isset($seller)) {
            $seller->name = $request->name;
            $seller->email = $request->email;
            $seller->save();
            return json_encode($seller);
            return response('OK', 200);
        }
    }

    public function showAllSales($seller_id)
    {
        $seller = Seller::find($seller_id);
        $sales = Sale::where('seller_id',$seller_id)->get();

        $vlrTotal = 0;
        foreach ($sales as $s){
            $vlrTotal+= $s['price'];
        }

        $comissao = $this->comissao($vlrTotal);
        return json_encode([
            "seller_id"=>$seller->id,
            "nome"=>$seller->name,
            "email"=>$seller->email,
            "vendas"=>$sales,
            "valor_total"=>$vlrTotal,
            "comissao"=> $comissao]);
    }

    public function comissao($price){
        $porcentagem = 6.5;
        $res = $price * ($porcentagem / 100);
        return $res;
    }

}
